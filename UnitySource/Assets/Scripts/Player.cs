﻿using UnityEngine;

namespace Assets
{
    public class Player : MonoBehaviour
    {
        public CharacterController2D Controller;

        private float _horizontalMove;
        private bool _jump = false;

        public float RunSpeed = 40f;

        // Start is called before the first frame update
        void Start()
        {
        
        }

        public void Update()
        {
            _horizontalMove = Input.GetAxisRaw("Horizontal") * RunSpeed;

            if (Input.GetButtonDown("Jump"))
            {
                _jump = true;
            }
        }

        void FixedUpdate()
        {
            Controller.Move(_horizontalMove * Time.fixedDeltaTime, false, _jump);
            _jump = false;
        }
    }
}
