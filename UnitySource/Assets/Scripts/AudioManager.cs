﻿using UnityEngine;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour
{
    static AudioManager current;

    [Header("Ambient Audio")]
    public AudioClip ambientClip;
    public AudioClip musicClip;

    [Header("Stings")]
    public AudioClip levelStingClip;
    public AudioClip winStingClip;

    [Header("Crab Audio")]
    public AudioClip[] walkStepClips;
    public AudioClip jumpClip;

    public AudioClip jumpVoiceClip;
    public AudioClip hehayVoiceClip;
    public AudioClip winVoiceClip;

    [Header("Mixer Groups")]
    public AudioMixerGroup ambientGroup;
    public AudioMixerGroup musicGroup;
    public AudioMixerGroup stingGroup;
    public AudioMixerGroup playerGroup;
    public AudioMixerGroup voiceGroup;

    AudioSource ambientSource;
    AudioSource musicSource;
    AudioSource stingSource;
    AudioSource playerSource;
    AudioSource voiceSource;

    void Awake()
    {
        if (current != null)
            Destroy(current);

        current = this;
        //DontDestroyOnLoad(gameObject);

        ambientSource = gameObject.AddComponent<AudioSource>() as AudioSource;
        musicSource = gameObject.AddComponent<AudioSource>() as AudioSource;
        stingSource = gameObject.AddComponent<AudioSource>() as AudioSource;
        playerSource = gameObject.AddComponent<AudioSource>() as AudioSource;
        voiceSource = gameObject.AddComponent<AudioSource>() as AudioSource;
        
        ambientSource.outputAudioMixerGroup = ambientGroup;
        musicSource.outputAudioMixerGroup = musicGroup;
        stingSource.outputAudioMixerGroup = stingGroup;
        playerSource.outputAudioMixerGroup = playerGroup;
        voiceSource.outputAudioMixerGroup = voiceGroup;

        StartLevelAudio();
    }

    void StartLevelAudio()
    {
        if (current.musicClip != null)
        {
            current.musicSource.clip = current.musicClip;
            current.musicSource.loop = true;
            current.musicSource.Play();
        }

        PlaySceneRestartAudio();
    }

    public static void UpdateWindSound(float percentage)
    {
        if (current == null || current.ambientClip == null)
            return;

        current.ambientSource.volume = percentage;

        if (!current.ambientSource.isPlaying)
        {
            current.ambientSource.clip = current.ambientClip;
            current.ambientSource.loop = true;
            current.ambientSource.Play();
        }
    }

    public static void StopWindSound()
    {
        if (current == null || current.ambientClip == null)
            return;

        current.ambientSource.Stop();
    }

    public static void PlayFootstepAudio()
    {
        if (current == null || current.playerSource.isPlaying || current.walkStepClips.Length <= 0)
            return;

        int index = Random.Range(0, current.walkStepClips.Length);

        current.playerSource.clip = current.walkStepClips[index];
        current.playerSource.Play();
    }

    public static void PlayJumpAudio()
    {
        if (current == null)
            return;

        if (current.jumpClip != null)
        {
            current.playerSource.clip = current.jumpClip;
            current.playerSource.Play();
        }

        if (current.jumpVoiceClip != null)
        {
            current.voiceSource.clip = current.jumpVoiceClip;
            current.voiceSource.Play();
        }
    }


    public static void PlayHehay()
    {
        if (current == null || current.hehayVoiceClip == null)
            return;

        current.voiceSource.clip = current.hehayVoiceClip;
        current.voiceSource.Play();
    }

    public static void PlaySceneRestartAudio()
    {
        if (current == null || current.levelStingClip == null)
            return;

        current.stingSource.clip = current.levelStingClip;
        current.stingSource.Play();
    }

    public static void PlayWonAudio()
    {
        if (current == null)
            return;

        current.ambientSource.Stop();

        if (current.winVoiceClip != null)
        {
            current.voiceSource.clip = current.winVoiceClip;
            current.voiceSource.Play();
        }

        if (current.winStingClip != null)
        {
            current.stingSource.clip = current.winStingClip;
            current.stingSource.Play();
        }
    }
}
