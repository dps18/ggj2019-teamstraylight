﻿using System.Collections.Generic;
using UnityEngine;
using Assets;
using TMPro;
using UnityEngine.SceneManagement;

public class GoalHandler : MonoBehaviour
{
    public TextMeshProUGUI EndGameMessageTextbox;

    private bool gameHasEnded;
    private bool endgameProcessed;

    private List<AccoutrementHandler> _collidingObjects;

    private EndGameUi _endGameUi;

    void Awake()
    {
        _collidingObjects = new List<AccoutrementHandler>();

        _endGameUi = FindObjectOfType<EndGameUi>();
        _endGameUi.gameObject.SetActive(false);

        gameHasEnded = false;
        endgameProcessed = false;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            var levelNumber = SceneManager.GetActiveScene().buildIndex;
            SceneManager.LoadScene(levelNumber);
        }
        if (gameHasEnded && !endgameProcessed)
            HandleEndGame();
        if (gameHasEnded && (Input.GetKeyDown(KeyCode.KeypadEnter) || Input.GetKeyDown(KeyCode.Return)))
        {
            var nextLevel = GameSettings.GetCurrentLevel() + 1;
            var isInitialized = GameSettings.IsInitialized();
            GameSettings.SetCurrentLevel(nextLevel);
            if(isInitialized && nextLevel < SceneManager.sceneCountInBuildSettings)
                SceneManager.LoadScene(nextLevel);
            else
                Application.Quit();
        }
    }

    void HandleEndGame()
    {
        endgameProcessed = true;

        _endGameUi.gameObject.SetActive(true);

        long score = 0;
        foreach (var collidingObject in _collidingObjects)
        {
            score += collidingObject.GetPointValueOfAttachedObjects();
        }

        var isLastLevel = GameSettings.GetCurrentLevel() == SceneManager.sceneCountInBuildSettings - 1;
        EndGameMessageTextbox.text = "You Win!\r\nScore: " + score + "\r\nPress Enter To Continue";
        if (isLastLevel)
            EndGameMessageTextbox.text += "\r\n\r\nTruly, home is where your family is";

        var playerScript = FindObjectOfType<Player>();
        playerScript.enabled = false;
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        var collectedItem = collision.gameObject.GetComponent<AccoutrementHandler>();
        if (collectedItem != null)
            _collidingObjects.Add(collectedItem);
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        var goal = collider.gameObject.GetComponent<Goal>();
        if (goal != null)
        {
            Debug.Log("Touched goal");
            gameHasEnded = true;
        }
    }

    void OnCollisionExit2D(Collision2D collision)
    {
        var collectedItem = collision.gameObject.GetComponent<AccoutrementHandler>();
        if (collectedItem != null)
            _collidingObjects.Remove(collectedItem);
    }
}
