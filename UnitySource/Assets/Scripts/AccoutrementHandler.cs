﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets
{
    public class AccoutrementHandler : MonoBehaviour
    {
        public float MoveSpeed;
        public float Offset = 0.001f;
        public bool GroundIsLava;
        public long PointValue = 0;

        private bool _grabbed;
        bool _hasBeenGrabbed;

        private Rigidbody2D _rigidbody2D;
        private Collider2D _collider2D;

        private List<AccoutrementHandler> _collidingObjects;

        // Use this for initialization
        void Start()
        {
            _grabbed = false;
            _hasBeenGrabbed = false;
            
            Offset = 0.5f;

            _rigidbody2D = GetComponent<Rigidbody2D>();
            _collider2D = GetComponent<Collider2D>();

            _collidingObjects = new List<AccoutrementHandler>();
        }

        // Update is called once per frame
        void Update()
        {
            var mouseScreenPos = Input.mousePosition;
            mouseScreenPos.z = 10;
            var mousePosition = Camera.main.ScreenToWorldPoint(mouseScreenPos);
            var mouseClicked = Input.GetMouseButtonDown(0);

            if (mouseClicked && (mousePosition - transform.position).magnitude <= Offset)
            {
                _grabbed = true;
                _hasBeenGrabbed = true;
                _rigidbody2D.constraints = RigidbodyConstraints2D.FreezeRotation;
                _rigidbody2D.transform.rotation = Quaternion.identity;
            }
            else if (Input.GetMouseButtonUp(0))
            {
                _grabbed = false;
                _rigidbody2D.constraints = RigidbodyConstraints2D.None;
            }

            if (_grabbed)
            {
                transform.position = new Vector3(mousePosition.x, mousePosition.y, transform.position.z);
            }
        }

        void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.gameObject.layer == 8 && GroundIsLava) // && _hasBeenGrabbed) // colliding with Platform
            {
                // TODO Add particle effect for disappearing accoutrements
                Destroy(gameObject);
            }
            else
            {
                var pointObject = collision.gameObject.GetComponent<AccoutrementHandler>();
                if (pointObject != null)
                    _collidingObjects.Add(pointObject);
            }
        }

        void OnCollisionExit2D(Collision2D collision)
        {
            var pointObject = collision.gameObject.GetComponent<AccoutrementHandler>();
            if (pointObject != null)
                _collidingObjects.Remove(pointObject);
        }

        public long GetPointValueOfAttachedObjects(List<AccoutrementHandler> visitedAccoutrements = null, long currentTotal = 0)
        {
            if(visitedAccoutrements == null)
                visitedAccoutrements = new List<AccoutrementHandler>();
            visitedAccoutrements.Add(this);

            currentTotal += PointValue;
            foreach (var collidingObject in _collidingObjects)
            {
                var instanceId = collidingObject.GetInstanceID();
                if (visitedAccoutrements.Any(a => a.GetInstanceID() == instanceId))
                    continue;
                currentTotal += collidingObject.GetPointValueOfAttachedObjects(visitedAccoutrements, currentTotal);
            }
            return currentTotal;
        }
    }
}
        