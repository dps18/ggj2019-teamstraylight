﻿using UnityEngine;

public class DebugWebHooks : MonoBehaviour
{
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha9))
        {
            AudioManager.PlayHehay();
        }
        if (Input.GetKeyDown(KeyCode.Alpha8))
        {
            ObjectSpawner.SpawnObject(ObjectSpawner.SpawnObjectType.Ant);
        }
        if (Input.GetKeyDown(KeyCode.Alpha7))
        {
            ObjectSpawner.SpawnObject(ObjectSpawner.SpawnObjectType.Bucket);
        }
        if (Input.GetKeyDown(KeyCode.Alpha6))
        {
            ObjectSpawner.SpawnObject(ObjectSpawner.SpawnObjectType.BeachBall);
        }
        if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            ObjectSpawner.SpawnObject(ObjectSpawner.SpawnObjectType.Balloon);
        }
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            WindManager.AddWind(-5.0f);
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            WindManager.StopWind();
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            WindManager.AddWind(5.0f);
        }
    }
}
