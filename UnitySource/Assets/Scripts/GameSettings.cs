﻿using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class GameSettings : MonoBehaviour
{
    private string GameId;
    private int CurrentLevel;
    public TextMeshProUGUI CodeText;

    private static GameSettings current;

    void Awake()
    {
        if (current != null)
        {
            Destroy(current.gameObject);
        }

        current = this;
        DontDestroyOnLoad(gameObject);

        var gameIdInt = Random.Range(1000, 9999);
        GameId = gameIdInt.ToString();

        CodeText.text = "http://tinyurl.com/playcrabgame \r\n" + "Enter code '" + GameId + "' to play along!";
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }

    public static string GetGameId()
    {
        if (current == null)
            return "";

        return current.GameId;
    }

    public static bool IsInitialized()
    {
        return current != null;
    }

    public static void SetCurrentLevel(int level)
    {
        if (current == null)
            return;
        current.CurrentLevel = level;
    }

    public static int GetCurrentLevel()
    {
        if (current == null)
            return 0;
        return current.CurrentLevel;
    }
}
