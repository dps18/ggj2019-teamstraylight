﻿using Assets;
using UnityEngine;
using System.Collections.Generic;

public class ObjectSpawner : MonoBehaviour
{
    public float MinX = 1.0f;
    public float MaxX = 5.0f;
    public float MinY = 0.0f;
    public float MaxY = 15.0f;
    private Transform _playerTransform;

    private static ObjectSpawner current;

    void Start()
    {
        if (current != null)
            return;

        var playerScript = FindObjectOfType<Player>();
        _playerTransform = playerScript.gameObject.transform;
        current = this;
    }

    public static void SpawnObject(SpawnObjectType type)
    {
        if (current == null)
            return;

        var currentPos = current._playerTransform.position;
        var x = Random.Range(currentPos.x + current.MinX, currentPos.x + current.MaxX);
        var y = Random.Range(currentPos.y + current.MinY, currentPos.y + current.MaxY);
        var newPos = new Vector3(x, y, currentPos.z);

        if (type == SpawnObjectType.Invalid || !_prefabNameMap.ContainsKey(type))
        {
            Debug.LogWarning("Tried to spawn unsupported type " + type);
            return;
        }
        var prefabName = _prefabNameMap[type];
        Instantiate(Resources.Load("Prefabs/" + prefabName), newPos, Quaternion.identity);
    }

    private static Dictionary<SpawnObjectType, string> _prefabNameMap = new Dictionary<SpawnObjectType, string>
    {
        {SpawnObjectType.Ant, "Ant"},
        {SpawnObjectType.Balloon, "Balloon" },
        {SpawnObjectType.BeachBall, "BeachBall" },
        {SpawnObjectType.Bucket, "BeachPail_flat" },
        {SpawnObjectType.BottleCap, "BottleCap" },
        {SpawnObjectType.BowlingBall, "BowlingBall" },
        {SpawnObjectType.Brick, "Brick" },
        {SpawnObjectType.CinderBlock, "CinderBlock" },
        {SpawnObjectType.ConchShell, "ConchShell" },
        {SpawnObjectType.Diamond, "Gem_Diamond" },
        {SpawnObjectType.Ruby, "Gem_Ruby" },
        {SpawnObjectType.Lego1, "LegoBrick_Long" },
        {SpawnObjectType.Lego2, "LegoBrick_Short" },
        {SpawnObjectType.SurfBoard, "SurfBoard" },
        {SpawnObjectType.Emerald, "Gem_Emerald" },
        {SpawnObjectType.Sapphire, "Gem_Sapphire" },
        {SpawnObjectType.Peanut, "Peanut" },
        {SpawnObjectType.Thimble, "Thimble" },
        {SpawnObjectType.TrafficCone, "TrafficCone" },
        {SpawnObjectType.Television, "TVset" },
    };

    public enum SpawnObjectType
    {
        Invalid = 0,
        Ant = 1,
        Balloon = 2,
        Bucket = 3,
        BeachBall = 4,
        BottleCap = 5,
        BowlingBall = 6,
        Brick = 7,
        CinderBlock = 8,
        ConchShell = 9,
        Diamond = 10,
        Ruby = 11,
        Lego1 = 12,
        Lego2 = 13,
        SurfBoard = 14,
        Emerald = 15,
        Sapphire = 16,
        Peanut = 17,
        Thimble = 18,
        TrafficCone = 19,
        Television = 20,
    }
}
