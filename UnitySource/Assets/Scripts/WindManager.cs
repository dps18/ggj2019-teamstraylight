﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindManager : MonoBehaviour
{
    public float CurrentWind = 0.0f;
    public float RigidBodyListRefreshRateInSeconds = 5.0f;
    public float MaxWindForce = 50.0f;

    private Rigidbody2D[] _rigidBodies;
    private DateTimeOffset _nextRigidBodyRefresh;

    private static WindManager current;

    void Start()
    {
        if (current != null)
            return;

        RefreshRigidBodyList();

        current = this;
    }

    void Update()
    {
        if (current == null)
            return;

        if (DateTimeOffset.Now < _nextRigidBodyRefresh)
            RefreshRigidBodyList();

        if (current.CurrentWind != 0.0f)
        {
            var windForce = new Vector3(current.CurrentWind, 0.0f, 0.0f);
            foreach (var rigidBody in current._rigidBodies)
            {
                rigidBody.AddForce(windForce);
            }
        }
    }

    // TODO: Would be awesome to do asynchronously...
    void RefreshRigidBodyList()
    {
        _rigidBodies = FindObjectsOfType<Rigidbody2D>();
        _nextRigidBodyRefresh = DateTimeOffset.Now.AddSeconds(RigidBodyListRefreshRateInSeconds);
    }

    public static void StopWind()
    {
        if (current == null)
            return;

        current.CurrentWind = 0.0f;
        AudioManager.StopWindSound();
    }

    public static void AddWind(float additonalWindForce)
    {
        if (current == null)
            return;

        current.CurrentWind += additonalWindForce;
        if (current.CurrentWind > current.MaxWindForce)
            current.CurrentWind = current.MaxWindForce;
        if (current.CurrentWind < -current.MaxWindForce)
            current.CurrentWind = -current.MaxWindForce;

        if (current.CurrentWind == 0)
            AudioManager.StopWindSound();
        else if(current.MaxWindForce != 0)
        {
            var percentage = Math.Abs(current.CurrentWind) / current.MaxWindForce;
            AudioManager.UpdateWindSound(percentage);
        }
    }
}
