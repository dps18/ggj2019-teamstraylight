﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using Debug = UnityEngine.Debug;

public class ProcessViewerRequests : MonoBehaviour
{
    public float CheckIntervalInSeconds = 5.0f;
    public float DelayBetweenRequestsInSeconds = 30.0f;

    private DateTimeOffset _nextCheckTime;
    private bool _checkingRequests;

    private const string CheckRequestUrlFormat = "http://3.17.143.133:80/api/game/{0}/process/next";
    private string _checkRequestUrl;

    void Start()
    {
        Debug.Log("Game ID is " + GameSettings.GetGameId());
        _checkRequestUrl = string.Format(CheckRequestUrlFormat, GameSettings.GetGameId());
        _nextCheckTime = DateTimeOffset.Now;
    }

	void Update ()
    {
        if (_checkingRequests || _nextCheckTime > DateTimeOffset.Now)
            return;
        _checkingRequests = true;
        StartCoroutine(CheckRequests());
    }

    IEnumerator CheckRequests()
    {
        var request = UnityWebRequest.Get(_checkRequestUrl);
        yield return request.SendWebRequest();

        if (request.isNetworkError || request.isHttpError)
            Debug.LogWarning(request.error);
        else
        {
            var json = request.downloadHandler.text;
            var viewerRequest = JsonUtility.FromJson<ViewerRequest>(json);
            ProcessViewerRequest(viewerRequest);
        }

        _checkingRequests = false;
    }

    private void ProcessViewerRequest(ViewerRequest viewerRequest)
    {
        var noRequest = false;
        switch (viewerRequest.RequestType)
        {
            case RequestType.HeyEh:
                AudioManager.PlayHehay();
                break;
            case RequestType.SpawnObject:
                var typeRaw = "";
                if(viewerRequest.Parameters != null && viewerRequest.Parameters.Length == 1)
                    typeRaw = viewerRequest.Parameters[0];
                int typeInt;
                var validType = int.TryParse(typeRaw, out typeInt);
                if (!validType || ((ObjectSpawner.SpawnObjectType) typeInt) == ObjectSpawner.SpawnObjectType.Invalid)
                    noRequest = true;
                else
                    ObjectSpawner.SpawnObject((ObjectSpawner.SpawnObjectType)typeInt);
                break;
            case RequestType.WindAdjust:
                var windSpeedRaw = "";
                if (viewerRequest.Parameters != null && viewerRequest.Parameters.Length == 1)
                    windSpeedRaw = viewerRequest.Parameters[0];
                float windSpeed;
                var validWindSpeed = float.TryParse(windSpeedRaw, out windSpeed);
                if (!validWindSpeed)
                    noRequest = true;
                else
                    WindManager.AddWind(windSpeed);
                break;
            case RequestType.WindStop:
                WindManager.StopWind();
                break;
            default:
                noRequest = true;
                break;
        }

        if (noRequest)
            _nextCheckTime = DateTimeOffset.Now.AddSeconds(CheckIntervalInSeconds);
        else
            _nextCheckTime = DateTimeOffset.Now.AddSeconds(DelayBetweenRequestsInSeconds);
    }

    public class ViewerRequest
    {
        public RequestType RequestType;
        public string[] Parameters;
        public DateTimeOffset DatePlaced;
    }

    public enum RequestType
    {
        Invalid = 0,
        None = 1,
        HeyEh = 2,
        SpawnObject = 3,
        WindAdjust = 4,
        WindStop = 5,
    }
}
