﻿using System.Collections.Generic;

namespace WebApi.Models
{
    public class PlayGameViewModel
    {
        public string GameId { get; set; }
        public Dictionary<int, string> SpawnOptions { get; set; }
    }
}