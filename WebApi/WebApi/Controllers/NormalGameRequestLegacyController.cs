﻿using System.Web.Http;
using Services.Models;
using Services.Services;

namespace WebApi.Controllers
{
    public class NormalGameRequestLegacyController: ApiController
    {
        private readonly IViewerRequestService _viewerRequestService;

        public NormalGameRequestLegacyController()
        {
            _viewerRequestService = ViewerRequestFactory.BuildViewerRequestService();
        }

        [Route("api/game/process/next")]
        [HttpGet]
        public ViewerRequest GetNextCommand()
        {
            var request = _viewerRequestService.GetNextRequest();
            return request;
        }
    }
}