﻿using System.Web.Http;
using Services.Models;
using Services.Services;

namespace WebApi.Controllers
{
    [RoutePrefix("api/game/{gameId}")]
    public class NormalGameRequestController: ApiController
    {
        private readonly IViewerRequestService _viewerRequestService;

        public NormalGameRequestController()
        {
            _viewerRequestService = ViewerRequestFactory.BuildViewerRequestService();
        }

        [Route("hehay")]
        [HttpGet]
        public void RequestEh(string gameId)
        {
            var request = new ViewerRequest(RequestType.HeyEh, gameId: gameId);
            _viewerRequestService.MakeRequest(request);
        }

        [Route("spawn/{spawnedObjectType}")]
        [HttpGet]
        public void RequestSpawn(string gameId, SpawnedObjectType spawnedObjectType)
        {
            var parameters = new[] {((int)spawnedObjectType).ToString()};
            var request = new ViewerRequest(RequestType.Spawn, parameters, gameId: gameId);
            _viewerRequestService.MakeRequest(request);
        }

        [Route("wind/adjust/{windAdjustment}")]
        [HttpGet]
        public void AdjustWind(string gameId, float windAdjustment)
        {
            var parameters = new[] {windAdjustment.ToString()};
            var request = new ViewerRequest(RequestType.WindAdjust, parameters, gameId: gameId);
            _viewerRequestService.MakeRequest(request);
        }

        [Route("wind/stop")]
        [HttpGet]
        public void StopWind(string gameId)
        {
            var request = new ViewerRequest(RequestType.WindStop, gameId: gameId);
            _viewerRequestService.MakeRequest(request);
        }

        [Route("process/next")]
        [HttpGet]
        public ViewerRequest GetNextCommand(string gameId)
        {
            var request = _viewerRequestService.GetNextRequest(gameId);
            return request;
        }
    }
}