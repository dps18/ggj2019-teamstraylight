﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Services.Models;
using WebApi.Models;

namespace WebApi.Controllers
{
    public class ViewerPlayPageController: Controller
    {
        [HttpGet]
        public ActionResult PlayGame(string gameId)
        {
            var spawnOptions = new Dictionary<int, string>();
            foreach (var spawnType in Enum.GetValues(typeof(SpawnedObjectType)))
            {
                if ((SpawnedObjectType) spawnType == SpawnedObjectType.Invalid)
                    continue;
                spawnOptions.Add((int)spawnType, $"{spawnType.ToString()}");
            }

            var model = new PlayGameViewModel
            {
                SpawnOptions = spawnOptions,
                GameId = gameId,
            };

            return View(model);
        }

        [HttpGet]
        public ActionResult StartGame()
        {
            return View();
        }
    }
}