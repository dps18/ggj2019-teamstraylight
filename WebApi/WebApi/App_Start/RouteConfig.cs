﻿using System.Web.Mvc;
using System.Web.Routing;

namespace WebApi
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(name: "Start", url: "play", defaults: new { controller = "ViewerPlayPage", action = "StartGame" });
            routes.MapRoute(name: "Play", url: "play/{gameId}", defaults: new {controller = "ViewerPlayPage", action = "PlayGame"});
        }
    }
}
