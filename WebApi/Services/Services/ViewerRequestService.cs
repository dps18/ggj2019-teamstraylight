﻿using System;
using Services.Models;
using Services.Models.Entities;
using Services.Repositories;

namespace Services.Services
{
    public interface IViewerRequestService
    {
        ViewerRequest GetNextRequest(string gameId = null);

        void MakeRequest(ViewerRequest request);
    }

    public class ViewerRequestService: IViewerRequestService
    {
        private readonly IViewerRequestRepository _repository;

        public ViewerRequestService(IViewerRequestRepository repository)
        {
            _repository = repository;
        }

        public ViewerRequest GetNextRequest(string gameId = null)
        {
            var request = _repository.GetNextRequest(gameId);
            if(request == null)
                return new ViewerRequest(RequestType.None);
            _repository.DeleteRequest(request.Id);
            var convertedRequest = ConvertFromEntity(request);
            return convertedRequest;
        }

        public void MakeRequest(ViewerRequest request)
        {
            var entity = ConvertToEntity(request);
            _repository.SaveRequest(entity);
        }
        
        private static ViewerRequest ConvertFromEntity(ViewerRequestEntity entity)
        {
            var result = new ViewerRequest((RequestType) entity.RequestType, entity.Parameters, entity.DatePlaced, entity.GameId);
            return result;
        }

        private static ViewerRequestEntity ConvertToEntity(ViewerRequest request)
        {
            var result = new ViewerRequestEntity
            {
                Id = Guid.NewGuid().ToString("D"),
                GameId = request.GameId,
                RequestType = (int)request.RequestType,
                Parameters = request.Parameters,
                DatePlaced = request.DatePlaced,
            };
            return result;
        }
    }
}
