﻿using System;
using MongoDB.Driver;
using Services.Repositories;

namespace Services.Services
{
    public static class ViewerRequestFactory
    {
        public static IViewerRequestService BuildViewerRequestService()
        {
            var settings = new MongoClientSettings
            {
                Server = MongoServerAddress.Parse("localhost"),
                ConnectTimeout = TimeSpan.FromSeconds(10),
            };
            var mongoClient = new MongoClient(settings);
            var repository = new MongoViewerRequestRepository(mongoClient);
            var service = new ViewerRequestService(repository);
            return service;
        }
    }
}
