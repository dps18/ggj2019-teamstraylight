﻿using MongoDB.Driver;
using Services.Models.Entities;

namespace Services.Repositories
{
    public class MongoViewerRequestRepository: IViewerRequestRepository
    {
        private readonly IMongoCollection<ViewerRequestEntity> _requestCollection;

        public MongoViewerRequestRepository(IMongoClient mongoClient)
        {
            var db = mongoClient.GetDatabase("ggj2019");
            _requestCollection = db.GetCollection<ViewerRequestEntity>("requests");
        }

        public ViewerRequestEntity GetNextRequest(string gameId = null)
        {
            var filterBuilder = Builders<ViewerRequestEntity>.Filter;
            FilterDefinition<ViewerRequestEntity> filter;
            if (string.IsNullOrEmpty(gameId))
                filter = filterBuilder.Exists(f => f.Id);
            else
                filter = filterBuilder.Eq(f => f.GameId, gameId);
            var sort = Builders<ViewerRequestEntity>.Sort.Ascending(s => s.DatePlaced);
            var oldestRequest = _requestCollection.Find(filter).Sort(sort).Limit(1).FirstOrDefault();
            return oldestRequest;
        }

        public void DeleteRequest(string id)
        {
            var filter = Builders<ViewerRequestEntity>.Filter.Eq(f => f.Id, id);
            _requestCollection.DeleteOne(filter);
        }

        public void SaveRequest(ViewerRequestEntity viewerRequest)
        {
            _requestCollection.InsertOne(viewerRequest);
        }
    }
}
