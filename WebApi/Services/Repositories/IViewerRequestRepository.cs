﻿using Services.Models.Entities;

namespace Services.Repositories
{
    public interface IViewerRequestRepository
    {
        ViewerRequestEntity GetNextRequest(string gameId = null);
        void DeleteRequest(string id);
        void SaveRequest(ViewerRequestEntity viewerRequest);
    }
}
