﻿using System;

namespace Services.Models.Entities
{
    public class ViewerRequestEntity
    {
        public string Id { get; set; }
        public string GameId { get; set; }
        public int RequestType { get; set; }
        public string[] Parameters { get; set; }
        public DateTimeOffset DatePlaced { get; set; }
    }
}
