﻿namespace Services.Models
{
    public enum SpawnedObjectType
    {
        Invalid = 0,
        Ant = 1,
        Balloon = 2,
        Bucket = 3,
        BeachBall = 4,
        BottleCap = 5,
        BowlingBall = 6,
        Brick = 7,
        CinderBlock = 8,
        ConchShell = 9,
        Diamond = 10,
        Ruby = 11,
        Lego1 = 12,
        Lego2 = 13,
        SurfBoard = 14,
        Emerald = 15,
        Sapphire = 16,
        Peanut = 17,
        Thimble = 18,
        TrafficCone = 19,
        Television = 20,
    }
}
