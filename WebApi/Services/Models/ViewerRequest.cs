﻿using System;

namespace Services.Models
{
    public class ViewerRequest
    {
        public ViewerRequest(RequestType requestType, string[] parameters = null, DateTimeOffset? datePlaced = null, string gameId = null)
        {
            RequestType = requestType;
            Parameters = parameters ?? new string[0];
            DatePlaced = datePlaced ?? DateTimeOffset.Now;
            GameId = gameId;
        }

        public RequestType RequestType { get; }
        public string[] Parameters { get; }
        public DateTimeOffset DatePlaced { get; }
        
        public string GameId { get; }
    }

    public enum RequestType
    {
        Invalid = 0,
        None = 1,
        HeyEh = 2,
        Spawn = 3,
        WindAdjust = 4,
        WindStop = 5,
    }
}
